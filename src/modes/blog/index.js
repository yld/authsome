function isPublished (fragment) {
  return fragment.published
}

function isOwner (user, object) {
  if (!object || !object.owners || !user) {
    return false
  }

  for (const ownerId of object.owners) {
    if (ownerId === user.id) {
      return true
    }
  }
}

function teamPermissions (user, operation, object) {
  if (!user || !Array.isArray(user.teams)) {
    return false
  }

  for (const team of user.teams) {
    if (team.teamType.permissions === 'create' &&
        team.object.id === object.id &&
        operation === 'create') {
      return true
    } else if (team.teamType.permissions === 'update' &&
        team.object.id === object.id &&
        operation === 'update') {
      return true
    }
  }

  return false
}

var blog = function (user, operation, object) {
  if (user && user.admin === true) return true

  let collection

  if (object.type === 'collection') {
    collection = object
    if (isOwner(user, collection)) {
      return true
    }

    if (teamPermissions(user, operation, collection)) {
      return true
    }
  } else if (object.type === 'fragment') {
    let fragment = object

    if (isPublished(fragment) && operation === 'read') {
      return true
    }

    if (isOwner(user, fragment)) {
      return true
    }

    if (teamPermissions(user, operation, fragment)) {
      return true
    }

    if (Array.isArray(fragment.parents)) {
      collection = fragment.parents[0]
      return teamPermissions(user, operation, collection)
    }
  } else if (object.type === 'user') {
    return user.id === object.id
  }

  return false
}

module.exports = blog
