import expect from 'expect.js'
import Authsome from '../../../src'
import blog from '../blog'

// CREATE UPDATE DELETE

let admin = {
  id: 'user_1',
  name: 'Admin user',
  admin: true,
  type: 'user'
}

let collection = {
  id: 'collection_1',
  name: 'Blog',
  owners: [admin.id],
  type: 'collection'
}

let teams = {
  teamContributors: {
    name: 'Contributors',
    permissions: 'create'
  },
  teamCoauthors: {
    name: 'Coauthors',
    permissions: 'update'
  }
}

let teamContributors = {
  id: 'team_1',
  object: collection,
  type: 'team',
  teamType: teams.teamContributors
}

let user2 = {
  id: 'user_2',
  name: 'User One',
  teams: [teamContributors],
  type: 'user'
}

let fragment = {
  id: 'fragment_1',
  title: 'Post',
  owners: [user2.id],
  parents: [collection],
  type: 'fragment'
}

let teamCoauthors = {
  id: 'team_2',
  object: fragment,
  type: 'team',
  teamType: teams.teamCoauthors
}

let user3 = {
  id: 'user_3',
  name: 'User Two',
  teams: [teamCoauthors],
  type: 'user'
}

let fragment2 = {
  id: 'fragment_2',
  title: 'Post',
  owners: [user3.id],
  parents: [collection],
  type: 'fragment'
}

let fragment3 = {
  id: 'fragment_2',
  title: 'Post',
  owners: [user3.id],
  parents: [collection],
  type: 'fragment',
  published: true
}

const authsome = new Authsome(blog, {
  teams: teams
})

// TODO: translate these tests, previously located in pubsweet-server
// it('reads all fragments', () => {
//   return api.users.authenticate.post(
//     fixtures.user
//   ).then(
//     token => api.fragments.get(collection, token)
//   ).then(
//     res => expect(res.body.length).toEqual(1)
//   )
// })

// it('updates a fragment owned by someone else', () => {
//   return api.users.authenticate.post(
//     fixtures.user
//   ).then(
//     token => api.fragments.put(
//       fragment.id, fixtures.fragment, collection, token
//     ).expect(STATUS.OK)
//   )
// })
// describe('published fragment', () => {
//   beforeEach(() => setNewFragment({ published: true }))

//   it('can read a fragment in a protected collection' +
//      ' if it is published', () => {
//     return api.fragments.get(collection).expect(STATUS.OK).then(
//       res => expect(res.body[0].id).toEqual(fragment.id)
//     )
//   })
// })

// describe('unpublished fragment', () => {
//   beforeEach(() => setNewFragment({ published: false }))

//   it('can not list unpublished fragments in a protected collection', () => {
//     return api.fragments.get(collection).expect(STATUS.OK).then(
//       res => expect(res.body).toEqual([])
//     )
//   })

// describe('admin', () => {
//   var otherUser

//   beforeEach(() => {
//     otherUser = new User(fixtures.otherUser)

//     return otherUser.save().then(
//       user => { otherUser = user }
//     )
//   })

//   afterEach(() => {
//     return User.find(otherUser.id).then(
//       user => user.delete()
//     ).catch(
//       noop // we might have already delete the user
//     )
//   })

//   it('can get a list of users', () => {
//     return api.users.authenticate.post(
//       fixtures.user
//     ).then(
//       token => api.users.get(null, token).expect(STATUS.OK)
//     ).then(
//       res => {
//         expect(res.body.users.length).toBe(2)
//         expect(res.body.users[0].username).not.toBe(undefined)
//       }
//     )
//   })

//   it('deletes a user', () => {
//     return api.users.authenticate.post(
//       fixtures.user
//     ).then(
//       token => api.users.del(otherUser.id, token).expect(STATUS.OK)
//     )
//   })
// })

// describe('unauthenticated user', () => {
//   it(
//     'can not get a list of users',
//     () => api.users.get().expect(STATUS.UNAUTHORIZED)
//   )

//   it('can not sign up as an admin directly', () => {
//     const fakeadmin = Object.assign(
//       {}, fixtures.otherUser, { admin: true }
//     )
//     return api.users.post(fakeadmin).expect(STATUS.CONFLICT)
//   })

//   it('can sign up', () => {
//     return api.users.post(
//       fixtures.otherUser
//     ).expect(
//       STATUS.CREATED
//     ).then(
//       res => {
//         expect(res.body.username).toBe(fixtures.otherUser.username)
//       }
//     )
//   })
// })

// describe('new user', () => {
//   let otherUser

//   beforeEach(() => {
//     return new User(fixtures.otherUser).save().then(
//       user => { otherUser = user }
//     )
//   })

//   afterEach(() => {
//     return User.find(otherUser.id).then(
//       user => user.delete()
//     ).catch(
//       noop // we might have already delete the user
//     )
//   })

//   it('cant log in with the wrong username', () => {
//     return api.users.authenticate.post({
//       username: 'wrongusername',
//       password: 'wrongpassword'
//     }, {
//       expect: false,
//       token: false
//     }).then(res => {
//       expect(res.statusCode).toEqual(STATUS.UNAUTHORIZED)
//     }
//     )
//   })

//   it('cant log in with the wrong password', () => {
//     return api.users.authenticate.post({
//       username: otherUser.username,
//       password: 'wrongpassword'
//     }, {
//       expect: false,
//       token: false
//     }).then(res => {
//       expect(res.statusCode).toEqual(STATUS.UNAUTHORIZED)
//     }
//     )
//   })

//   it('can not get a list of users', () => {
//     return api.users.authenticate.post(
//       fixtures.otherUser
//     ).then(
//       token => api.users.get(null, token).expect(STATUS.FORBIDDEN)
//     )
//   })

//   it('can not delete other users', () => {
//     return api.users.authenticate.post(
//       fixtures.otherUser
//     ).then(
//       token => api.users.del(userId, token).expect(STATUS.FORBIDDEN)
//     )
//   })

//   it('can not get other users', () => {
//     return api.users.authenticate.post(
//       fixtures.otherUser
//     ).then(
//       token => api.users.get(userId, token).expect(STATUS.FORBIDDEN)
//     )
//   })

//   it('can get itself', () => {
//     return api.users.authenticate.post(
//       fixtures.otherUser
//     ).then(
//       token => api.users.get(otherUser.id, token).expect(STATUS.OK)
//     ).then(
//       res => {
//         expect(res.body.id).toBe(otherUser.id)
//         expect(res.body.username).toBe(fixtures.otherUser.username)
//       }
//     )
//   })

//   it('can not make itself admin', () => {
//     const newself = Object.assign(
//       { id: otherUser.id, admin: true }, fixtures.otherUser
//     )

//     return api.users.authenticate.post(
//       fixtures.otherUser
//     ).then(
//       token => api.users.put(
//         otherUser.id, newself, token
//       ).expect(
//         STATUS.FORBIDDEN
//       )
//     )
//   })

//   it('updates itself', () => {
//     const newself = Object.assign({}, fixtures.updatedUser)

//     return api.users.authenticate.post(
//       fixtures.otherUser
//     ).then(
//       token => api.users.put(
//         otherUser.id, newself, token
//       ).expect(
//         STATUS.OK
//       )
//     )
//   })

//   it('authenticates an updated user', () => {
//     const newself = Object.assign({}, fixtures.updatedUser)

//     return api.users.authenticate.post(
//       fixtures.otherUser
//     ).then(
//       token => api.users.put(
//         otherUser.id, newself, token
//       ).expect(
//         STATUS.OK
//       )
//     ).then(() =>
//       api.users.authenticate.post(fixtures.updatedUser)
//     )
//   })

//   it('persists an updated user', () => {
//     const newself = Object.assign({}, fixtures.updatedUser)

//     return api.users.authenticate.post(
//       fixtures.otherUser
//     ).then(
//       token => api.users.put(
//         otherUser.id, newself, token
//       ).expect(
//         STATUS.OK
//       ).then(
//         () => token
//       )
//     ).then(
//       token => api.users.get(otherUser.id, token).expect(STATUS.OK)
//     ).then(
//       res => {
//         expect(res.body.id).toBe(otherUser.id)
//         expect(res.body.username).toBe(fixtures.updatedUser.username)
//       }
//     )
//   })

//   it('user can delete itself', () => {
//     const newself = Object.assign({}, fixtures.updatedUser)

//     return api.users.authenticate.post(
//       fixtures.otherUser
//     ).then(
//       token => api.users.put(
//         otherUser.id, newself, token
//       ).expect(
//         STATUS.OK
//       )
//     ).then(
//       () => api.users.authenticate.post(fixtures.updatedUser)
//     ).then(
//       token => api.users.del(otherUser.id, token).expect(STATUS.OK)
//     )
//   })
// })

//   it('should display an initially empty list of teams if user is admin', () => {
//     return api.users.authenticate.post(
//       fixtures.adminUser
//     ).then(
//       token => api.teams.get(token).expect(STATUS.OK)
//     ).then(
//       res => expect(res.body).toEqual([])
//     )
//   })

//   it('should display the existing teams if user is admin', () => {
//     return new Team(
//       teamFixture
//     ).save().then(
//       () => api.users.authenticate.post(fixtures.adminUser)
//     ).then(
//       token => api.teams.get(token).expect(STATUS.OK)
//     ).then(
//       res => {
//         let team = res.body[0]
//         expect(team.teamType.name).toEqual(contributors.name)
//         expect(team.members).toEqual([])
//       }
//     )
//   })

//   it('should not allow listing all teams if user is not an admin', () => {
//     return api.users.authenticate.post(
//       fixtures.user
//     ).then(
//       token => api.teams.get(token).expect(STATUS.FORBIDDEN)
//     )
//   })
// })

// describe('Teams API - per collection or fragment', () => {
//   describe('Collection teams', () => {
//     describe('owners', () => {
//       let collectionId
//       let otherUserId
//       let teamId
//       let team

//       beforeEach(() => {
//         return cleanDB().then(
//           () => new User(fixtures.user).save()
//         ).then(
//           user => {
//             let collection = new Collection(fixtures.collection)
//             collection.setOwners([user.id])
//             return collection.save()
//           }
//         ).then(
//           collection => { collectionId = collection.id }
//         ).then(
//           () => new User(fixtures.updatedUser).save()
//         ).then(
//           otherUser => { otherUserId = otherUser.id }
//         ).then(
//           () => { team = cloneDeep(teamFixture) }
//         )
//       })

//       it('can display an initially empty list of teams', () => {
//         return api.users.authenticate.post(
//           fixtures.user
//         ).then(
//           token => api.teams.get(token, collectionId).expect(STATUS.OK)
//         ).then(
//           res => expect(res.body).toEqual([])
//         )
//       })

//       it('can add a team with a team member to a collection and this team member can then create fragments', () => {
//         team.name = 'Test team'
//         team.members = [otherUserId]
//         team.object = {
//           id: collectionId,
//           type: 'collection'
//         }

//         return api.users.authenticate.post(
//           fixtures.user
//         ).then(
//           token => api.teams.post(
//             team, collectionId, token
//           ).expect(
//             STATUS.CREATED
//           )
//         ).then(
//           res => {
//             teamId = res.body.id
//             expect(res.body.name).toEqual(team.name)
//           }
//         ).then(
//           () => api.users.authenticate.post(fixtures.updatedUser)
//         ).then(
//           token => api.fragments.post(
//             fixtures.fragment, collectionId, token
//           ).expect(
//             STATUS.CREATED
//           )
//         )
//       })

//       it('can remove a team member and that removed team member can no longer create fragments', () => {
//         team.name = 'Test team'
//         team.members = [otherUserId]
//         team.object = {
//           id: collectionId,
//           type: 'collection'
//         }

//         return api.users.authenticate.post(
//           fixtures.user
//         ).then(
//           token => api.teams.post(
//             team, collectionId, token
//           ).expect(
//             STATUS.CREATED
//           ).then(
//             res => [res, token]
//           )
//         ).then(
//           ([res, token]) => {
//             teamId = res.body.id
//             team.members = []
//             return api.teams.put(
//               team, collectionId, teamId, token
//             ).expect(
//               STATUS.OK
//             )
//           }
//         ).then(
//           () => api.users.authenticate.post(fixtures.updatedUser)
//         ).then(
//           token => api.fragments.post(
//             fixtures.fragment, collectionId, token
//           ).expect(
//             STATUS.FORBIDDEN
//           )
//         )
//       })
//     })

//     describe('non-owners', () => {
//       let collectionId

//       beforeEach(() => {
//         return cleanDB().then(
//           () => new User(fixtures.user).save()
//         ).then(
//           user => {
//             let collection = new Collection(fixtures.collection)
//             collection.owners = []
//             return collection.save()
//           }
//         ).then(
//           collection => { collectionId = collection.id }
//         )
//       })

//       it('should not be authorized to see teams for a collection', () => {
//         return api.users.authenticate.post(
//           fixtures.user
//         ).then(
//           token => api.teams.get(token, collectionId).expect(STATUS.FORBIDDEN)
//         )
//       })
//     })
//   })


// Authenticated API tests, to be translated/moved:
// describe('authenticated api', function () {
//   var otherUser
//   var collection

//   beforeEach(() => {
//     // Create collection with admin user and one non-admin user
//     return dbCleaner().then(
//       createBasicCollection
//     ).then(
//       (userAndCol) => { collection = userAndCol.collection }
//     ).then(
//       () => {
//         // Create another user without any roles
//         otherUser = new User(fixtures.updatedUser)
//         return otherUser.save()
//       }
//     )
//   })

//   afterEach(dbCleaner)

//   it(`fails to create a fragment in a protected
//       collection if authenticated as user without permissions`, () => {
//     return api.users.authenticate.post(
//       fixtures.updatedUser
//     ).then(
//       (token) => {
//         return api.fragments.post(
//           fixtures.fragment, collection, token
//         ).expect(
//           STATUS.FORBIDDEN
//         )
//       }
//     )
//   })

//   describe('a non-admin user with a contributor role', () => {
//     beforeEach(() => {
//       return setTeamForCollection(
//         [otherUser.id],
//         collection,
//         fixtures.contributorTeam
//       )
//     })

//     afterEach(() => {
//       return setTeamForCollection(
//         [],
//         collection,
//         fixtures.contributorTeam
//       )
//     })

//     it('creates a fragment in a protected collection', () => {
//       return api.users.authenticate.post(
//         fixtures.updatedUser
//       ).then(
//         token => {
//           return api.fragments.post(
//             fixtures.fragment, collection, token
//           ).expect(
//             STATUS.CREATED
//           )
//         }
//       ).then(
//         res => {
//           expect(res.body.owners).toContainEqual({
//             id: otherUser.id,
//             username: otherUser.username
//           })
//         }
//       )
//     })

//     describe('a fragment owned by the same user', () => {
//       var fragment

//       beforeEach(() => {
//         fragment = new Fragment(fixtures.fragment)
//         fragment.setOwners([otherUser.id])
//         return fragment.save()
//       })

//       afterEach(() => {
//         return fragment.delete()
//       })

//       it('updates a fragment in a protected collection if an owner', () => {
//         return api.users.authenticate.post(
//           fixtures.updatedUser
//         ).then(
//           (token) => {
//             return api.fragments.put(
//               fragment.id,
//               fixtures.updatedFragment,
//               collection,
//               token
//             ).expect(
//               STATUS.OK
//             )
//           }
//         )
//       })
//     })

//     describe('actions on a fragment owned by a different user', () => {
//       var fragment

//       beforeEach(() => {
//         const Fragment = require('../src/models/Fragment')
//         fragment = new Fragment(fixtures.fragment)
//         fragment.setOwners([otherUser.id])
//         return fragment.save()
//       })

//       afterEach(() => {
//         return fragment.delete()
//       })

//       it('cannot read a fragment in a protected collection if it is not published', () => {
//         return api.users.authenticate.post(
//           fixtures.updatedUser
//         ).then(
//           token => api.fragments.get(collection, token).expect(STATUS.OK)
//         ).then(
//           res => expect(res.body).toEqual([])
//         )
//       })

//       it('cannot update a fragment in a protected collection', () => {
//         return api.users.authenticate.post(
//           fixtures.updatedUser
//         ).then(
//           token => {
//             return api.fragments.put(
//               fixtures.updatedFragment, collection, token
//             ).expect(
//               STATUS.UNAUTHORIZED
//             )
//           }
//         )
//       })
//     })
//   })

//   describe('a non-admin user with a reader role', () => {
//     beforeEach(() => {
//       return setTeamForCollection(
//         [otherUser.id],
//         collection,
//         fixtures.readerTeam
//       )
//     })

//     afterEach(() => {
//       return setTeamForCollection(
//         [],
//         collection,
//         fixtures.readerTeam
//       )
//     })

//     it('can not create a fragment', () => {
//       return api.users.authenticate.post(
//         fixtures.updatedUser
//       ).then(
//         token => {
//           return api.fragments.post(
//             fixtures.fragment, collection, token
//           ).expect(
//             STATUS.FORBIDDEN
//           )
//         }
//       )
//     })

//     it('can read a fragment', function () {
//       return api.users.authenticate.post(
//         fixtures.updatedUser
//       ).then(
//         token => {
//           return api.fragments.get(
//             collection,
//             token
//           )
//         }
//       )
//     })
//   })

//   it('fails to create a fragment in the protected collection if not authenticated', function () {
//     return api.fragments.post(
//       fixtures.fragment, collection
//     ).expect(
//       STATUS.UNAUTHORIZED
//     )
//   })

//   it('fails to create a fragment in the protected collection if authentication wrong', function () {
//     return api.fragments.post(
//       fixtures.fragment, collection, 'wrong'
//     ).expect(
//       STATUS.UNAUTHORIZED
//     )
//   })

describe('Blog mode', function () {
  describe('admin', function () {
    it('should be able to delete fragments owned by someone else', function () {
      var permission = authsome.can(admin, 'delete', fragment)
      expect(permission).to.eql(true)
    })

    it('should be able to create fragments', function () {
      var permission = authsome.can(admin, 'create', fragment)
      expect(permission).to.eql(true)
    })
  })

  describe('member of team contributors', function () {
    it('should be able to create fragments', function () {
      var permission = authsome.can(user2, 'create', fragment)
      expect(permission).to.eql(true)
    })

    it('should not be able to delete fragments which are owned by someone else', function () {
      var permission = authsome.can(user2, 'create', fragment)
      expect(permission).to.eql(true)
    })

    it('should be able to update fragments owned by them', function () {
      var permission = authsome.can(user2, 'update', fragment)
      expect(permission).to.eql(true)
    })

    it('should not be able to update fragments owned by other users', function () {
      var permission = authsome.can(user2, 'update', fragment2)
      expect(permission).to.eql(false)
    })
  })

  describe('member of team coauthors', function () {
    it('should be able to update the fragment', function () {
      var permission = authsome.can(user3, 'update', fragment)
      expect(permission).to.eql(true)
    })
  })

  describe('member of public', () => {
    it('should be able to access published fragments', () => {
      const permission = authsome.can(null, 'read', fragment3)
      expect(permission).to.eql(true)
    })

    it('should not be able to accessed unpublished fragments', () => {
      const permission = authsome.can(null, 'read', fragment)
      expect(permission).to.eql(false)
    })
  })
})
