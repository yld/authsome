// WARNING: Used purely for testing reasons
// Everything is allowed for everyone everywhere (if they are 'registered')
// and if the object has a property called 'public', and its truthy, it can be read

var test = function (user, operation, object) {
  if (user || (object && object.public)) {
    return true
  } else {
    return false
  }
}

module.exports = test
