import expect from 'expect.js'
import Authsome from '../../../src'
import test from '../test'

let user1 = {
  id: 'user1',
  name: 'Just A User',
  type: 'user'
}

let user2 = {
  id: 'user_2',
  name: 'User Two',
  type: 'user'
}

let collection = {
  id: 'collection_1',
  name: 'Blog',
  owners: [user1.id],
  type: 'collection'
}

let fragment = {
  id: 'fragment_1',
  title: 'Post',
  owners: [user2.id],
  parents: [collection],
  type: 'fragment'
}

const authsome = new Authsome(test)

describe('Free for all mode', function () {
  describe('just a random user', function () {
    it('should be able to delete fragments owned by someone else', function () {
      var permission = authsome.can(user1, 'delete', fragment)
      expect(permission).to.eql(true)
    })

    it('should be able to create fragments', function () {
      var permission = authsome.can(user1, 'create', fragment)
      expect(permission).to.eql(true)
    })

    it('should be able to do whatever', function () {
      var permission = authsome.can(user1, 'whatever', undefined)
      expect(permission).to.eql(true)
    })
  })

  describe('unauthenticated user', function () {
    it('should not be able to do whatever', function () {
      var permission = authsome.can(undefined, 'whatever', undefined)
      expect(permission).to.eql(false)
    })
  })
})
