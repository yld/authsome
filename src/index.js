'use strict'

class Authsome {
  constructor (mode, config) {
    this.mode = mode
    if (config) {
      this.teams = config.teams
    }

    if (!this.mode) {
      this.mode = 'freefornone'
      this.teams = undefined
    }
  }

  can (user, operation, object) {
    if (this.mode === 'freefornone') {
      return false
    } else {
      return this.mode(user, operation, object)
    }
  }
}

module.exports = Authsome
