# Authsome

Flexible team-based authorization module. This software is in an alpha stage.

# Overview

Authsome is team-based, which means that authorization is determined based on team membership. A team consists of a group of members and belongs to one of the at team types configured at load time.
A team is always based around an object (the object of the authorization request). A team can also be conditionally active, i.e. only active if the current state of the object matches the requirement for team activation.
Best to explain this in an example. See Science Blogger mode example below.

# Modes

Modes are exchangeable mechanisms of authorization (e.g. built in example modes: blog, journal, and noon).

# API example

If Authsome configuration is:

```
let config = {
  mode: require('./src/modes/blog'),
  teams: {
    teamContributors: {
      name: 'Contributors',
      permissions: 'create'
    },
    teamCoauthors: {
      name: 'Coauthors',
      permissions: 'update'
    }
  }
}
```

Then Authsome can be created like so:

```
let authsome = new Authsome(
  config.mode,
  { teams: config.teams }
)
```

And then used like so:

```
authsome.can(user, operation, object)
```

or:

```
authsome.can(currentUser, 'update', fragment)
```

The currently configured modes then take over and decide on the authorization, returning true or false.

# Science Blogger example

In [Science Blogger](https://gitlab.coko.foundation/pubsweet/science-blogger), we have two types of teams. One is a 'contributor' type, which allows you to create blogposts for the blog. The other is a 'coauthor' team type, which allows you to update a specific blogpost (write it with someone). There is no conditional activity of teams, and only these two team types.

When your managing teams, you create a new team with a certain type, and a certain object (in the case of Contributors, you would chose the blog object as the object of the team). You can then add members to this team, and those members can then ask (and receive) authorization to create blogposts for the blog.

Authsome can be used on both the backend (for authorizing API requests, e.g. [for creating collections in PubSweet](https://gitlab.coko.foundation/pubsweet/core/blob/d6c330b724d6f3ca9a7460a36ec61ebfa8287da5/api/routes/api_collection.js#L23-37)), and frontend (for showing/hiding UI that relates to an authorization request, e.g. a [Create Blogpost button.](https://gitlab.coko.foundation/pubsweet/core/blob/d6c330b724d6f3ca9a7460a36ec61ebfa8287da5/app/components/PostsManager/PostsManager.jsx#L36-38))

# Future plans

- [x] Take individual modes and make them into modules, that you can plug and play.
- [ ] Introduce concept of protected attributes/properties. All attributes/properties that could have an effect on the authorization result should be protected from being change with insufficient authorization (and by doing so changing the authorization result.). This is essential and until this is handled this should be considered alpha/prototype stage software.

